
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>

#define NO_PHILOSOPHERS 5
#define LEFT(id) (id >= 1 ? id-1 : NO_PHILOSOPHERS-1)
#define RIGHT(id) ((id+1) % NO_PHILOSOPHERS)

enum philosopher_state {
	HUNGRY,
	EATING,
	THINKING
};

pthread_mutex_t cs = PTHREAD_MUTEX_INITIALIZER;
sem_t s[NO_PHILOSOPHERS];
volatile enum philosopher_state states[NO_PHILOSOPHERS] = {[0 ... NO_PHILOSOPHERS-1] = THINKING};

void test(size_t id) {
	if ((states[id] == HUNGRY) && (states[LEFT(id)] != EATING)
	 && (states[RIGHT(id)] != EATING)) {
		 states[id] = EATING;
		 sem_post(&s[id]);
	 }
}

void take_forks(size_t id) {
	pthread_mutex_lock(&cs);
	states[id] = HUNGRY;
	test(id);
	pthread_mutex_unlock(&cs);

	sem_wait(&s[id]);
}

void put_forks(size_t id) {
	pthread_mutex_lock(&cs);
	states[id] = THINKING;
	test(LEFT(id));
	test(RIGHT(id));
	pthread_mutex_unlock(&cs);
}

void think(size_t id) {
	size_t r = ((unsigned int)(((double) rand() / (double)RAND_MAX)*10))+1;

	printf("Philosopher %zd is thinking\n", id);

	usleep(r*500);
}

void eat(size_t id) {
	size_t r = ((unsigned int)(((double) rand() / (double)RAND_MAX)*10))+1;

	printf("Philosopher %zd is eating\n", id);

	usleep(r*500);
}

void* philosopher(void *arg) {
	size_t id = (size_t) arg;

	while(1) {
		think(id);
		take_forks(id);
		eat(id);
		put_forks(id);
	}

	return 0;
}

int main(int argc, char** argv) {
	pthread_t threads[NO_PHILOSOPHERS];

	// create philosophers
	for (size_t i = 0; i < NO_PHILOSOPHERS; i++) {
		// initialize semaphore with 0 because
		// the philosopher check decided, if the thread is able to run
		sem_init(&s[i], 0, 0);

		pthread_create(&(threads[i]), NULL, philosopher, (void*)i);
 	}

	sleep(5);

	printf("Stop dining philosophers\n");
	fflush(stdout);

	return 0;
}
