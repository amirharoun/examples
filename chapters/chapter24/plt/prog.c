#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>
#include "foo.h"

extern void call_foo(int arg);

static __inline__ unsigned long long rdtsc(void)
{
	unsigned hi, lo;
	__asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
	return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}

int main(int argc, char **argv)
{
	unsigned long long start1, start2, end1, end2;

	start1 = rdtsc();
	call_foo(42);
	end1 = rdtsc();

	start2 = rdtsc();
	call_foo(42);
	end2 = rdtsc();

	printf("CPU ticks for the first call : %lld\n", end1-start1);
	printf("CPU ticks for the second call: %lld\n", end2-start2); 

	return 0;
}
